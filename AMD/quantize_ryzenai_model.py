"""
Use RyzenAI Vitis AI to qunatize the ONNX model.
"""
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms, datasets
from onnxruntime.quantization import CalibrationDataReader, QuantType, QuantFormat, CalibrationMethod, quantize_static
import vai_q_onnx

class TinyImageNetDataSet:
    def __init__(self, data_dir, **kwargs):
        super().__init__()
        self.vld_path = data_dir + '/val'
        self.setup("fit")

    def setup(self, stage: str):
        transform = transforms.Compose(
            [transforms.Resize(256), transforms.CenterCrop(224), transforms.ToTensor()]
        )
        self.val_dataset = datasets.ImageFolder(root=self.vld_path, transform=transform)

class PytorchResNetDataset(Dataset):
    def __init__(self, dataset):
        self.dataset = dataset

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        sample = self.dataset[index]
        input_data = sample[0]
        label = sample[1]
        return input_data, label

def create_dataloader(data_dir, batch_size):
    tiny_imagenet_dataset = TinyImageNetDataSet(data_dir)
    val_set = tiny_imagenet_dataset.val_dataset
    benchmark_dataloader = DataLoader(PytorchResNetDataset(val_set), batch_size=batch_size, drop_last=True)
    return benchmark_dataloader

class ResnetCalibrationDataReader(CalibrationDataReader):
    def __init__(self, data_dir: str, batch_size: int = 16):
        super().__init__()
        self.iterator = iter(create_dataloader(data_dir, batch_size))

    def get_next(self) -> dict:
        try:
            images, labels = next(self.iterator)
            return {"data": images.numpy()}
        except Exception:
            return None

def resnet_calibration_reader(data_dir, batch_size=1):
    return ResnetCalibrationDataReader(data_dir, batch_size=batch_size)

def main():
    input_model_path = r"resnet50v2_fp32_batch1_opset13.onnx"
    output_model_path = r"resnet50v2_int8_batch1_opset13.onnx"
    calibration_dataset_path = "data/tiny-imagenet-200"

    dr = resnet_calibration_reader(calibration_dataset_path)

    print("Starting quantizing...")
    try:
        vai_q_onnx.quantize_static(
            input_model_path,
            output_model_path,
            dr,
            quant_format=vai_q_onnx.QuantFormat.QDQ,
            calibrate_method=vai_q_onnx.PowerOfTwoMethod.MinMSE,
            activation_type=vai_q_onnx.QuantType.QUInt8,
            weight_type=vai_q_onnx.QuantType.QInt8,
            enable_dpu=True,
            extra_options={'ActivationSymmetric': True}
        )
    except Exception as e:
        print(e)
    print('Calibrated and quantized model saved at:', output_model_path)

if __name__ == '__main__':
    main()