import requests
import zipfile
import os

# Download Tiny ImageNet dataset
url = 'http://cs231n.stanford.edu/tiny-imagenet-200.zip'
data_dir = 'data/'

# Make sure the directory exists
os.makedirs(data_dir, exist_ok=True)

# Download and extract the dataset
response = requests.get(url)
zip_path = os.path.join(data_dir, 'tiny-imagenet-200.zip')
with open(zip_path, 'wb') as f:
    f.write(response.content)

with zipfile.ZipFile(zip_path, 'r') as zip_ref:
    zip_ref.extractall(data_dir)

# Remove the zip file after extraction
os.remove(zip_path)

# Now you can use the dataset
from torchvision import datasets, transforms
from torch.utils.data import DataLoader

# Define the transformation for the data
transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
])

# Load the training and validation datasets
train_dataset = datasets.ImageFolder(root=os.path.join(data_dir, 'tiny-imagenet-200/train'), transform=transform)
val_dataset = datasets.ImageFolder(root=os.path.join(data_dir, 'tiny-imagenet-200/val'), transform=transform)

# Create the DataLoaders
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=32, shuffle=False)

# Example usage of DataLoader
for images, labels in train_loader:
    print(images.shape, labels.shape)
    break
