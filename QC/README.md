# QNN

## Env Set Up
Create the qnn environment
```
conda env create -f environment.yml
```

Create the qnn environment in x64 env
```
conda env create --platform osx-64 -f environment.yml
```

Update eht qnn environment
```
conda env update -f environment.yml
```

## Prepare the Onnx Model
The operation set should be at least version 11.

[Onnxruntime QNN](https://onnxruntime.ai/docs/execution-providers/QNN-ExecutionProvider.html)