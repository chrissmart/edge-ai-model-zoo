"""
get calibration dataset for quantization.
"""

import numpy as np
import onnxruntime
from onnxruntime.quantization import CalibrationDataReader
import cv2

class DataReader(CalibrationDataReader):
    def __init__(self, model_path: str):
        self.enum_data = None
        session = onnxruntime.InferenceSession(model_path, providers=['CPUExecutionProvider'])
        inputs = session.get_inputs()
        self.data_list = []

        # Generate float32 inputs
        input_image = cv2.imread(r"sloth.jpg")
        preprocessed_image = cv2.resize(input_image, (224, 224))
        preprocessed_image = np.array(preprocessed_image).transpose(2, 0, 1)
        img_data = preprocessed_image.astype("float32")
        mean_vec = np.array([0.485, 0.456, 0.406])
        stddev_vec = np.array([0.229, 0.224, 0.225])
        norm_img_data = np.zeros(img_data.shape).astype("float32")
        for i in range(img_data.shape[0]):
            norm_img_data[i,:,:] = (img_data[i,:,:]/255 - mean_vec[i]) / stddev_vec[i]        
        norm_img_data = norm_img_data.reshape(1, 3, 224, 224).astype("float32")
        input_data = {inp.name : norm_img_data for inp in inputs}
        for _ in range(10):
            self.data_list.append(input_data)
            
        self.datasize = len(self.data_list)

    def get_next(self):
        if self.enum_data is None:
            self.enum_data = iter(
                self.data_list
            )
        return next(self.enum_data, None)

    def rewind(self):
        self.enum_data = None
