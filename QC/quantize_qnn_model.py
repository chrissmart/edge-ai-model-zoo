"""
Quantize the Onnx model to run it on QC CPU and NPU.

Reference: Onnxruntime QNN
"""

import os
import numpy as np
from onnxruntime.quantization import QuantType, quantize
from onnxruntime.quantization.execution_providers.qnn import get_qnn_qdq_config, qnn_preprocess_model
import data_reader

MODEL_ROOT_PATH = "resnet50v2"

if __name__ == "__main__":
    input_model_path = os.path.join(MODEL_ROOT_PATH, "resnet50v2_opset11_batch1.onnx")
    output_model_path = os.path.join(MODEL_ROOT_PATH, "resnet50v2.qdq.onnx")
    qnn_data_reader = data_reader.DataReader(input_model_path)

    # Pre-process the original float32 model
    preproc_model_path = os.path.join(MODEL_ROOT_PATH, "resnet50v2.preproc.onnx")
    model_changed = qnn_preprocess_model(input_model_path, preproc_model_path)
    model_to_quantize = preproc_model_path if model_changed else input_model_path

    # Generate a suitable quantization configuration for this model.
    # Note that we're choosing to use uint16 activations and uint8 weights.
    qnn_config = get_qnn_qdq_config(model_to_quantize,
                                    qnn_data_reader,
                                    activation_type=QuantType.QUInt16,  # uint16 activations
                                    weight_type=QuantType.QUInt8)       # uint8 weights

    # Quantize the model.
    quantize(model_to_quantize, output_model_path, qnn_config)

    # run the quantized model

    options = onnxruntime.SessionOptions()
    # (Optional) Enable configuration that raises an exception if the model can't be
    # run entirely on the QNN HTP backend.
    options.add_session_config_entry("session.disable_cpu_ep_fallback", "1")
    session = onnxruntime.InferenceSession(output_model_path,
                                        sess_options=options,
                                        providers=["QNNExecutionProvider"],
                                        provider_options=[{"backend_path": "QnnHtp.dll"}]) # Provide path to Htp dll in QNN SDK

    for i in range(100):
        input0 = np.ones((1,3,224,224), dtype=np.float32)
        result = session.run(None, {"data": input0})
        print(result)
