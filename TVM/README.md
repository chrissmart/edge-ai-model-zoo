# TVM

## ARM-Based MAC OS

### Install Required SW
**Step 1. Install Git adn Build Automation Tool**
* git
* cmake
```
brew install gcc git
```
**Step 2. Install Compilers**
* gcc
* llvm
```
brew install cmake
brew install llvm
brew link llvm --force # link llvm installation to the correct path
```

### Install TVM
**Step 1. Create a ARM-Based Python 3.8 Env**
```
CONDA_SUBDIR=osx-arm64 conda create -n tvm python=3.8
conda activate tvm

conda config --env --set subdir osx-arm64
```
**Step 2. Pull TVM Repo to Install TVM**
```
git clone --recursive https://github.com/apache/tvm tvm
git submodule init
git submodule update
cd tvm
```
Under `tvm\cmake\config.cmake`, set LLVM search available by setting `set(USE_LLVM ON)` in the `config.cmake`.
![alt text](cmake_config.png)
Create a build folder and copy `config.cmake` to it. Next, use cmake to create Makefile and use make to build the TVM project.
```
mkdir build
cp cmake/config.cmake build
cd build
cmake ..
make -j4 # make run Makefile and -j4 means run maximum 4 jobs at the same time.
```

**Step 3. Install Required Python Packages**.
```
pip install -r requirements.txt
```

* TypeError: callback must be an instance of `TrainingCallback`.
```
pip install git+https://github.com/optuna/optuna.git
```
* other settings
```
export MACOSX_DEPLOYMENT_TARGET=10.9
export ARCHFLAGS="-arch arm64"
export PYTHONPATH=/Users/chrischien/ML/edge-ai-model-zoo/TVM/tvm/python
```

### Get an ONNX Model
```
wget https://github.com/onnx/models/raw/b9a54e89508f101a1611cd64f4ef56b9cb62c7cf/vision/classification/resnet/model/resnet50-v2-7.onnx
```

### Others
Show the default hardware target and host cup.
```
llc --version
```


Reference:
1. [How to Install TVM in MacOS](https://pyshine.com/How-to-install-TVM-in-MacOS/)