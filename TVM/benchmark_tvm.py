"""
This script compares the latency of different AI inference engines.
* TVM
* TVMC
* Onnxruntime
"""
import pandas as pd
import numpy as np
import time
from scipy.stats import f_oneway
from scipy import stats
import matplotlib.pyplot as plt
import onnx
import onnxruntime as ort
import tvm
from tvm import relay
from tvm.contrib.graph_executor import GraphModule
from tvm.driver import tvmc

MODEL_PATH = "../artifacts/models/resnet50-v2-7.onnx"
input_shape = (1, 3, 224, 224)
input_data = np.random.uniform(-1, 1, size=input_shape).astype("float32")
input_dict = {"data": input_data}
inference_times = 100

# Onnxruntime as inference engine
ort_infer_latency_records = []
ort_session = ort.InferenceSession(MODEL_PATH)
input_name = ort_session.get_inputs()[0].name
input_dtype = ort_session.get_inputs()[0].type
for i in range(inference_times):
    start_time = time.time()
    infer_result = ort_session.run(None, {input_name: input_data})
    end_time = time.time()
    ort_execution_time = end_time - start_time
    print(f"Onnxruntime inference process execution time: {ort_execution_time:.4f} seconds")
    ort_infer_latency_records.append(ort_execution_time)

ort_infer_latency_records = np.array(ort_infer_latency_records)
ort_infer_result = np.argmax(infer_result[0])

# TVM as inference engine
tvm_infer_latency_records = []
onnx_model = onnx.load(MODEL_PATH)
shape_dict = {input_name: input_shape}
mod, params = relay.frontend.from_onnx(onnx_model, shape_dict)
target = "llvm"
with tvm.transform.PassContext(opt_level=3):
    lib = relay.build(mod, target=target, params=params)
ctx = tvm.cpu()
module = GraphModule(lib["default"](ctx))
module.set_input(input_name, input_data)

for i in range(inference_times):
    start_time = time.time()
    module.run()
    end_time = time.time()
    tvm_execution_time = end_time - start_time
    print(f"TVM inference execution time: {tvm_execution_time:.4f} seconds")
    tvm_infer_latency_records.append(tvm_execution_time)

tvm_infer_latency_records = np.array(tvm_infer_latency_records)
tvm_infer_result = np.argmax(module.get_output(0).asnumpy())

# TVMC as inference engine
tvmc_infer_latency_records = []
relay_model = tvmc.load(MODEL_PATH)
package = tvmc.compile(relay_model, target="llvm")

for i in range(inference_times):
    start_time = time.time()
    infer_result = tvmc.run(package, device="cpu", inputs=input_dict) # run the compiled model on the target hardware # device options: CPU, Cuda, CL, Metal, Vulkan
    end_time = time.time()
    tvmc_execution_time = end_time - start_time
    print(f"TVM inference process execution time: {tvmc_execution_time:.4f} seconds")
    tvmc_infer_latency_records.append(tvmc_execution_time)

tvmc_infer_latency_records = np.array(tvmc_infer_latency_records)
tvmc_infer_result = np.argmax(infer_result.outputs['output_0'])

assert ort_infer_result == tvm_infer_result == tvm_infer_result, "inference result is not the same."

# Run the hypothesis testing and visualization
plt.hist(ort_infer_latency_records, bins=50, alpha=0.5, label='ort', color='blue')
plt.hist(tvm_infer_latency_records, bins=50, alpha=0.5, label='tvm', color='green')
plt.hist(tvmc_infer_latency_records, bins=50, alpha=0.5, label='tvmc', color='red')

plt.title('Inference Latency Distribution')
plt.xlabel('Latency (sec)')
plt.ylabel('Frequency')
plt.legend()
plt.show()

# hypothesis testing
infer_latency_data = {"ort": ort_infer_latency_records,
        "tvm": tvm_infer_latency_records,
        "tvmc": tvmc_infer_latency_records}
df_infer_latency = pd.DataFrame(infer_latency_data)
f_statistic, p_value = f_oneway(df_infer_latency["ort"], df_infer_latency["tvm"], df_infer_latency["tvmc"])
print("F-statistic:", f_statistic)
print("p-value:", p_value)

alpha = 0.05
if p_value < alpha:
    print('Reject the null hypothesis: There is a significant difference between the groups.')
else:
    print('Fail to reject the null hypothesis: There is no significant difference between the groups.')

# Perform independent t-test
t_statistic, p_value = stats.ttest_ind(df_infer_latency["ort"], df_infer_latency["tvm"], equal_var=True)
print('t-statistic:', t_statistic)
print('p-value:', p_value)

alpha = 0.05
if p_value < alpha:
    print('Reject the null hypothesis: There is a significant difference between the two groups.')
else:
    print('Fail to reject the null hypothesis: There is no significant difference between the two groups.')