"""
Convert onnx model to IR model.
"""

import openvino.runtime as ov

FP32_MODEL_PATH = r"artifacts/models/resnet50/resnet50-v2-7.onnx"
IR_OUTPUT_DIR = "artifacts/models/resnet50/ir_model"
XML_PATH = f"{IR_OUTPUT_DIR}/resnet50-v2-7.xml"
BIN_PATH = f"{IR_OUTPUT_DIR}/resnet50-v2-7.bin"

core = ov.Core()
model = core.read_model(FP32_MODEL_PATH)
ov.serialize(model, XML_PATH, BIN_PATH)
print(f"IR model saved to {XML_PATH} and {BIN_PATH}")