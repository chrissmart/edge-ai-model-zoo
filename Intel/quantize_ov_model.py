"""
Convert the model precision using:
* OpenVINO
"""

import nncf
import openvino.runtime as ov
import torch
from torchvision import datasets, transforms

FP32_MODEL_PATH = r"artifacts/model/resnet50/resnet50-v2-7.onnx"
IR_OUTPUT_DIR = "artifacts/model/resnet50/ir_model"
XML_PATH = f"{IR_OUTPUT_DIR}/resnet50-v2-7.xml"
BIN_PATH = f"{IR_OUTPUT_DIR}/resnet50-v2-7.bin"

# Prepare the model
core = ov.Core()
model = core.read_model("artifacts/models/resnet50/ir_model/resnet50-v2-7.xml")

# Prepare the calibration dataset
val_dataset = datasets.ImageFolder("data/tiny-imagenet-200/val", transform=transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor()]))
dataset_loader = torch.utils.data.DataLoader(val_dataset, batch_size=1)
def transform_fn(data_item):
    images, _ = data_item
    return images
calibration_dataset = nncf.Dataset(dataset_loader, transform_fn)

# Run the quantization pipeline
quantized_model = nncf.quantize(model, calibration_dataset)

ov.serialize(quantized_model, "artifacts/models/resnet50/ir_model/quantized_resnet50-v2-7.xml", "artifacts/models/resnet50/ir_model/quantized_resnet50-v2-7.bin")