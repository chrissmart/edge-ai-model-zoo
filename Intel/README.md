# Intel OpenVINO

## Environment Set Up
Create the env
```
conda env create -f environment.yml
```

Update the qnn environment
```
conda env update -f environment.yml
```
