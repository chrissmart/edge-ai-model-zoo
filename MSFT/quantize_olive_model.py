"""
Convert the model precision using:
* Olive
"""
from olive.workflows import run as olive_run
olive_run(r"MSFT/olive_quant_to_fp16.json")
