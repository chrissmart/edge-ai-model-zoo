"""
Convert the model precision using:
* Onnxruntime
"""
import onnx
import onnxruntime as ort
from onnx import version_converter, helper
from onnxruntime.quantization import quantize_dynamic, quantize_static, QuantType, QuantFormat, shape_inference

from onnxruntime.quantization import QuantFormat, QuantType

FP32_MODEL_PATH = r"resnet50v2\resnet50v2_batch1.onnx"
INT8_MODEL_PATH = r"resnet50v2\resnet50v2_batch1_int8.onnx"
OPSET11_MODEL_PATH = r"resnet50v2\resnet50v2_opset11_batch1.onnx"

model = onnx.load(FP32_MODEL_PATH)
converted_model = version_converter.convert_version(model, 19)
onnx.save(converted_model, OPSET11_MODEL_PATH)
print(f"Converted model saved to {OPSET11_MODEL_PATH}")

quantized_model = quantize_dynamic(
    OPSET11_MODEL_PATH,
    INT8_MODEL_PATH,
    weight_type=QuantType.QInt8
)

print(f"Quantized model saved to {INT8_MODEL_PATH}")