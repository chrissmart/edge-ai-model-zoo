"""
convert the onnx model operation set version
"""

import os
import onnx
from onnx import version_converter

MODEL_ROOT_PATH = "artifacts/models/resnet50"
FP32_MODEL_PATH = os.path.join(MODEL_ROOT_PATH, "resnet50-v2-7.onnx")
CONVERTED_MODEL_PATH = os.path.join(MODEL_ROOT_PATH, "resnet50-v2-7-opset11.onnx")

model = onnx.load(FP32_MODEL_PATH)

OPSET_VERSION = 11
converted_model = version_converter.convert_version(model, OPSET_VERSION)
print("Converted model operation set is ", converted_model.opset_import[0])

onnx.save(converted_model, CONVERTED_MODEL_PATH)
print(f"Converted model saved to {CONVERTED_MODEL_PATH}")