"""
Adjust onnx model arch.
For example, change the input batch size.
"""
import onnx

desired_batch_dim = 4
MODEL_PATH = r"resnet50v2_batch1.onnx"
MODEL_OUTPUT_PATH = rf"resnet50v2_batch{desired_batch_dim}.onnx"
model = onnx.load(MODEL_PATH)

# ============================================================================================================
def change_onnx_model_input_dim(model, desired_batch_dim):
    inputs = model.graph.input
    for input in inputs:
        if input.name == "data":
            dim1 = input.type.tensor_type.shape.dim[0]
            dim1.dim_value = desired_batch_dim
    return model
model = change_onnx_model_input_dim(model, desired_batch_dim=desired_batch_dim)
# ============================================================================================================

onnx.save(model, MODEL_OUTPUT_PATH)
